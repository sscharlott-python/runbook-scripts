# script
jwt = "@@{calm_jwt}@@"
vm_ip = "@@{address}@@"
prism_ip = "@@{prism_ip}@@"
blueprint_id = "@@{calm_blueprint_uuid}@@"

base_url = 'https://{0}:9440/api/nutanix/v3/'.format(prism_ip)
  
def delete_call(api_url):
  headers = { 'Authorization': 'Bearer {}'.format(jwt) }
  r = urlreq(api_url, verb='DELETE', headers=headers, verify=False)
  resp = json.loads(r.content)
  return resp

api_url = base_url + "endpoints/@@{endpoint_uuid}@@"
delete_call(api_url)

